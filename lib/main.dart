import 'package:flutter/material.dart';

void main() {
  runApp(MiCard());
}

class MiCard extends StatelessWidget {
  const MiCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        // appBar: AppBar(
        //   title: Center(child: Text("Mi Card")),
        //   backgroundColor: Colors.pink[700],
        // ),
        body: SafeArea(
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.pink[800]!,
                  Colors.pink[900]!,
                ],
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: double.infinity,
                ),
                CircleAvatar(
                  radius: 100,
                  backgroundImage: const AssetImage('assets/profile_icon.png'),
                ),
                Text(
                  'Wren',
                  style: TextStyle(
                    color: Colors.amber[50],
                    fontWeight: FontWeight.w700,
                    fontFamily: 'OpenDyslexic',
                    fontStyle: FontStyle.italic,
                    fontSize: 40,
                  ),
                ),
                Text(
                  '  person 🙋🏻‍♀️',
                  style: TextStyle(
                    color: Colors.yellow[50],
                    fontFamily: 'OpenDyslexic',
                    fontSize: 20,
                    fontWeight: FontWeight.w100,
                  ),
                ),
                SizedBox(
                  height: 25,
                  width: 300,
                  child: Divider(
                    color: Colors.yellowAccent,
                  ),
                ),
                ContactCards(
                  text: '(555)555-5555',
                  icon: Icons.phone,
                ),
                ContactCards(
                  text: 'wren@murena.io',
                  icon: Icons.email,
                ),
                ContactCards(
                  text: 'wren.fun',
                  icon: Icons.language,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ContactCards extends StatelessWidget {
  const ContactCards(
      {Key? key, this.text = 'null', this.icon = Icons.broken_image})
      : super(key: key);
  final String text;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 70),
      color: Colors.white,
      child: InkWell(
        splashColor: Colors.pink[300]!.withAlpha(120),
        onTap: () {
          debugPrint('Card tapped. 🃏');
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 7),
          child: ListTile(
            leading: Icon(
              icon,
              color: Colors.pink[800],
            ),
            title: Text(
              text,
              style: TextStyle(
                color: Colors.pink[800],
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
